package mc2.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;
import color_matrix.ColorMatrix;

class Mc2Action implements IAction {

    public var name(default, null): String = Obfu.raw("mc2");
    public var isVerbose(default, null): Bool = false;

    public function new() {
    }

    public function run(ctx: IActionContext): Bool {
        var cx = ctx.getOptFloat(Obfu.raw("x")).toNullable();
        var cy = ctx.getOptFloat(Obfu.raw("y")).toNullable();
        var xr = ctx.getOptFloat(Obfu.raw("xr")).toNullable();
        var yr = ctx.getOptFloat(Obfu.raw("yr")).toNullable();

        var sid = ctx.getOptInt(Obfu.raw("sid")).toNullable();
        var n = ctx.getString(Obfu.raw("n"));

        var scaleX = ctx.getOptFloat(Obfu.raw("sx")).or(_ => 100);
        var scaleY = ctx.getOptFloat(Obfu.raw("sy")).or(_ => 100);
        var flip = ctx.getOptBool(Obfu.raw("flip")).or(_ => false);
        var rotation = ctx.getOptFloat(Obfu.raw("rot"));
        var frame = ctx.getOptInt(Obfu.raw("f"));

        var alpha = ctx.getOptFloat(Obfu.raw("a"));
        var color = ctx.getOptFloat(Obfu.raw("c")).toNullable();
        var saturation = ctx.getOptFloat(Obfu.raw("s")).toNullable();
        var value = ctx.getOptFloat(Obfu.raw("v")).toNullable();
        var replace = ctx.getOptBool(Obfu.raw("replace")).toNullable();
        var front = ctx.getOptBool(Obfu.raw("front")).toNullable();

        var x = ctx.getGame().flipCoordReal(xr == null ? ctx.getGame().root.Entity.x_ctr(cx) : xr);
        var y = yr == null ? ctx.getGame().root.Entity.y_ctr(cy) : yr;
        if (ctx.getGame().fl_mirror)
            x += ctx.getGame().root.Data.CASE_WIDTH;

        var sprite;
        if (replace == null) {
            replace = true;
        }
        if (front == null) {
            front = false;
        }
        if ((sid != null && replace == true) || front) {
            ctx.killById(sid);
        }
        if (replace || ctx.getMc(sid) == null) {
            if (front) {
                var frontsprite = ctx.getGame().depthMan.attach(n, ctx.getGame().root.Data.DP_INTERF + 2);
                frontsprite._x = x;
                frontsprite._y = y;
                ctx.registerMc(sid, frontsprite);
            }
            else {
                merlin.Actions.MC.run(ctx);
            }

            var spriteList = ctx.getGame().world.view.mcList;
            sprite = spriteList[spriteList.length - 1];
        }
        else {
            sprite = ctx.getMc(sid);
        }

        if (replace == false) {
            sprite._x = x;
            sprite._y = y;
        }

        if (ctx.getGame().fl_mirror) {
            scaleX = -scaleX;
        }
        if (flip) {
            scaleX = -scaleX;
        }
        sprite._yscale = scaleY;
        sprite._xscale = scaleX;

        rotation.map(rot => sprite._rotation = rot);
        frame.map(f => {
            sprite.gotoAndStop(f);
            untyped sprite.sub.stop();
        });

        alpha.map(a => sprite._alpha = a);

        if (saturation == null) {
            saturation = 1;
        }
        if (value == null) {
            value = 1;
        }

        if (color != null) {
            sprite.filters = [ColorMatrix.fromHsv(color, saturation, value).toFilter()];
        }

        return false;
    }
}
