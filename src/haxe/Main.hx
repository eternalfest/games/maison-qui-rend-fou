import bottom_bar.BottomBarInterface;
import etwin.Obfu;
import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import better_script.Players;
import bottom_bar.BottomBar;
import bottom_bar.modules.BottomBarModuleFactory;
import maison.Maison;
import maison.LevelCounter;
import mc2.Mc2;
import maxmods.Misc;
import atlas.Atlas;

@:build(patchman.Build.di())
class Main {
    public static function main(): Void {
        Patchman.bootstrap(Main);
    }

    public function new(
        bugfix: Bugfix,
        debug: Debug,
        gameParams: GameParams,
        noNextLevel: NoNextLevel,
        bottomBar: BottomBar,
        maison: Maison,
        mc2: Mc2,
        misc: Misc,
        players: Players,
        atlasNoFireball: atlas.props.NoFireball,
        atlas: atlas.Atlas,
        merlin: Merlin,
        patches: Array<IPatch>,
        hf: Hf
    ) {
        BottomBarModuleFactory.get().addModule(Obfu.raw("LevelCounter"), function(data: Dynamic) return new LevelCounter(data, maison));
        Patchman.patchAll(patches, hf);
    }
}
