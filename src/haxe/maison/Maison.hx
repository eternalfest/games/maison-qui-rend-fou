package maison;

import etwin.Obfu;
import maison.actions.LevelCounterAction;

import patchman.module.GameEvents;
import patchman.Ref;
import patchman.IPatch;
import etwin.ds.FrozenArray;
import merlin.IAction;

@:build(patchman.Build.di())
class Maison {
    public var globalLevelCounter: Int;
    public var differentLevelCounter: Int;
    public var updateLevelCounters: Bool;
    public var active: Bool;
    private var gameEvents: GameEvents;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;
    @:diExport
    public var action(default, null): IAction;

    public function new(gameEvents: GameEvents) {
        this.gameEvents = gameEvents;

        var patches = [
            Ref.auto(hf.SpecialManager.warpZone).replace(function(hf: hf.Hf, self: hf.SpecialManager, w: Int): Void {
                self.game.fxMan.attachAlert(hf.Lang.get(34));
            }),
            Ref.auto(hf.mode.GameMode.onLevelReady).before(function(hf: hf.Hf, self: hf.mode.GameMode):Void {
                if (self.currentDim != 0) {
                    this.globalLevelCounter += 1;
                    if (!self.world.isVisited())
                        this.differentLevelCounter += 1;
                }
                this.updateLevelCounters = true;
            }),
            Ref.auto(hf.mode.Adventure.saveScore).replace(function(hf: hf.Hf, self: hf.mode.Adventure): Void {
                var maxLevel: Int = self.dimensions[0].currentId;
                var itemsTaken: Array<Int> = self.getPicks2();
                var items: Map<String, Int> = new Map();
                for (i in 0...itemsTaken.length) {
                    if (itemsTaken[i] != null) {
                        items.set(Std.string(i), itemsTaken[i]);
                    }
                }
                this.gameEvents.endGame(new ef.api.run.SetRunResultOptions(maxLevel == 1, maxLevel, self.savedScores, items, {
                    var obj = {};
                    Obfu.setField(obj, "levelCount", this.globalLevelCounter);
                    Obfu.setField(obj, "diffLevelCount", this.differentLevelCounter);
                    obj;
                }));
            })

        ];
        this.patches = FrozenArray.from(patches);

        this.action = new LevelCounterAction(this);
    }
}
