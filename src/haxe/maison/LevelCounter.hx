package maison;

import merlin.value.MerlinFloat;
import merlin.Merlin;
import patchman.DebugConsole;
import hf.FxManager;
import etwin.Obfu;
import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;

class LevelCounter extends BottomBarModule {
    public var maison: Maison;
    public var x: Int;

    public var globalLevelSprite: Dynamic;
    public var globalLevelCounterIcon: Dynamic;
    public var differentLevelSprite: Dynamic;
    public var differentLevelCounterIcon: Dynamic;

    private function createStandardLabel(bottomBar:BottomBarInterface, x: Int) {
        var label: Dynamic = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        label.field._visible = false;
        label.label._visible = false;
        label.field._width = 400;
        label.field._xscale = 115;
        label.field._yscale = 115;
        label.field._x = x;
        label.field._y = 499;
        bottomBar.game.root.FxManager.addGlow(label, 7366029, 2);
        return label;
    }

    public function new(module: Map<String, Dynamic>, maison: Maison) {
        this.maison = maison;
        this.x = module[Obfu.raw("x")];
        this.maison.globalLevelCounter = 0;
        this.maison.differentLevelCounter = 0;
        this.maison.active = false;
    }

    public override function init(bottomBar:BottomBarInterface): Void {

        this.maison.globalLevelCounter = 0;
        this.globalLevelSprite = this.createStandardLabel(bottomBar, 0);
        this.globalLevelCounterIcon = bottomBar.game.root.Std.attachMC(bottomBar.mc, Obfu.raw('icon1'), bottomBar.game.manager.uniq++);
        this.globalLevelCounterIcon._x = this.x + 40;
        this.globalLevelCounterIcon._y = -16;
        this.globalLevelCounterIcon._xscale = 100;
        this.globalLevelCounterIcon._yscale = 100;
        this.globalLevelCounterIcon._visible = false;
        Merlin.setGlobalVar(bottomBar.game, Obfu.raw("LEVEL_COUNT"), new MerlinFloat(0));

        this.maison.differentLevelCounter = 0;
        this.differentLevelSprite = this.createStandardLabel(bottomBar, 0);
        this.differentLevelCounterIcon = bottomBar.game.root.Std.attachMC(bottomBar.mc, Obfu.raw('icon2'), bottomBar.game.manager.uniq++);
        this.differentLevelCounterIcon._x = this.x + 40 + 15 + 40;
        this.differentLevelCounterIcon._y = -16;
        this.differentLevelCounterIcon._xscale = 100;
        this.differentLevelCounterIcon._yscale = 100;
        this.differentLevelCounterIcon._visible = false;
        Merlin.setGlobalVar(bottomBar.game, Obfu.raw("DIFF_LEVEL_COUNT"), new MerlinFloat(0));

        this.update(bottomBar);
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        if (this.maison.updateLevelCounters) {
            this.globalLevelSprite.field._visible = this.maison.active;
            this.globalLevelSprite.field.text = this.maison.globalLevelCounter;
            this.globalLevelSprite.field._x = this.x - 5 * (this.maison.globalLevelCounter > 9 ? 1 : 0) - 5 * (this.maison.globalLevelCounter > 99 ? 1 : 0);
            this.globalLevelCounterIcon._visible = this.maison.active;
            Merlin.setGlobalVar(bottomBar.game, Obfu.raw("LEVEL_COUNT"), new MerlinFloat(this.maison.globalLevelCounter));
            
            this.differentLevelSprite.field._visible = this.maison.active;
            this.differentLevelSprite.field.text = this.maison.differentLevelCounter;
            this.differentLevelSprite.field._x = this.x + 42 + 15 - 5 * (this.maison.differentLevelCounter > 9 ? 1 : 0) - 5 * (this.maison.differentLevelCounter > 99 ? 1 : 0);
            this.differentLevelCounterIcon._visible = this.maison.active;
            Merlin.setGlobalVar(bottomBar.game, Obfu.raw("DIFF_LEVEL_COUNT"), new MerlinFloat(this.maison.differentLevelCounter));

            this.maison.updateLevelCounters = false;
        }
        return true;

    }
}
