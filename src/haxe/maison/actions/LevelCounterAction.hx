package maison.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class LevelCounterAction implements IAction {
  public var name(default, null): String = Obfu.raw("levelCounter");
  public var isVerbose(default, null): Bool = false;

  private var mod: Maison;

  public function new(mod: Maison) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: hf.mode.GameMode = ctx.getGame();

    var active = ctx.getBool(Obfu.raw("active"));
    if (active) {
      this.mod.active = true;
      this.mod.globalLevelCounter = 1;
      this.mod.differentLevelCounter = 1;
    }
    else {
      this.mod.active = false;
    }
    this.mod.updateLevelCounters = true;
    return false;
  }
}
